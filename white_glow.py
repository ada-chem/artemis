#!/usr/bin/python

import time
import board
import neopixel
import numpy as np
import sys

num_pixels = 64 + 24
pixel_pin = board.D18
ORDER = neopixel.GRBW

pixels = neopixel.NeoPixel(
    pixel_pin,
    num_pixels,
    brightness=1,
    auto_write=True,
    pixel_order=ORDER,
    bpp=4
)

# while True:
#     for i in range(0, 255, 2):
#         for j in range(num_pixels):
#             pixels[j] = (0, 0, 0, i+1)
#         pixels.show()
#     for i in range(255, 0, -2):
#         for j in range(num_pixels):
#             pixels[j] = (0, 0, 0, i+1)
#         pixels.show()

while True:
    val = input()
    pix = int(float(val) * 255)
    pixels.fill((0,0,0,pix))