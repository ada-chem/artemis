import time
import board
import neopixel
import numpy as np

num_pixels = 64 + 24

pixels = neopixel.NeoPixel(
    board.D18,
    num_pixels,
    brightness=1,
    auto_write=True,
    pixel_order=neopixel.GRBW,
    bpp=4
)

pixels.fill((0,0,0,0))