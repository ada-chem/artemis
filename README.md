# Artemis

This is the lighting control software for the Ada Artemis imaging and microscope station.  Uses Python 3.7.  See [bit.ly/ada-artemis](bit.ly/ada-artemis) for more.