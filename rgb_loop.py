import time
import board
import neopixel
import numpy as np

num_pixels = 64 + 24
pixel_pin = board.D18
ORDER = neopixel.GRBW

pixels = neopixel.NeoPixel(
    pixel_pin,
    num_pixels,
    brightness=1,
    auto_write=True,
    pixel_order=ORDER,
    bpp=4
)

while True:
    pixels.fill((0, 0, 0, 255))
    input()
    pixels.fill((255, 0, 0, 0))
    input()
    pixels.fill((0, 255, 0, 0))
    input()
    pixels.fill((0, 0, 255, 0))
    input()
    pixels.fill((0, 0, 0, 0))
    input()
